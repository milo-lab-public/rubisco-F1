# Form I rubisco exploration
This repository is accompanying the paper "A systematic exploration of bacterial form I rubisco maximal carboxylation rates" by Benoit de Pins, Lior Greenspoon, Yinon M. Bar-On, Melina Shamshoum, Roee Ben-Nissan, Eliya Milshtein, Dan Davidi, Itai Sharon, Oliver Mueller-Cajar, Elad Noor and Ron Milo
https://www.weizmann.ac.il/plants/Milo/home 

## Rubisco sequence collection
Collection and annotation of rubisco sequences are detailed in the following link: https://github.com/SharonLab/RuBisCO-Annotation

## Repository contents
- **generate_cluster_representatives.ipynb**: code that was used to cluster rubisco sequences. 
- **all_rates.ipynb/photo_chemo.ipynb/carbox_noncarbox.ipynb/acyano_bcyano_CCMproteo.ipynb/rates_per_round.ipynb/raf1.ipynb/rates_lit_our_lab.ipynb/map.ipynb/environment.ipynb/green_red_anaero.ipynb/salinity.ipynb/pH.ipynb/ox_sensit.ipynb/temperature.ipynb/photo_chemo-thermophilesversion.ipynb/carbox_noncarbox-thermophilesversion.ipynb/acyano_bcyano_CCMproteo-thermophilesversion.ipynb/make_hexadecamers.ipynb/RMSD_LSU_SSU.ipynb/RMSD_AS.ipynb/random_forests.ipynb/cell_size.ipynb**: codes that were used to analyse the measured rubisco rates and the modeled structures.
- **data**: rubisco sequences, modeled structures, measured rates and associated metadata
- **bin**: usearch clustering algorithm
- **outputs**: result of the clustering and of the RMSD measurements
- **plots**: plots resulting from the analysis
